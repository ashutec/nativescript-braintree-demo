const braintree = require("braintree");


var gateway = braintree.connect({
  environment: braintree.Environment.Sandbox,
  merchantId: "5w53fft33r2542f5",
  publicKey: "tpcgyh9m4s9wrgbb",
  privateKey: "1aa8105ecd7b446c47591b84c3555dd0"
});
//get token
const getToken = async (req, res) => {
  gateway.clientToken.generate({}, (err, response) => {
    res.status(200).send({ token: response.clientToken });
  });
}
//fetch customers
const customerFind = async (req, res) => {
  let newTransaction = gateway.customer.search(function(search) {
    search.id().is("104963314");
  }, function (error, result) {
    if (result) {
      res.status(200).send(result);
    } else {
      res.status(500).send(error);
    }
  });
}

const checkout = async (req, res) => {
  let nonceFromTheClient = req.body.paymentMethodNonce;
  let newTransaction = gateway.transaction.sale({
    amount: req.body.amount,
    paymentMethodNonce: nonceFromTheClient,
    options: {
      submitForSettlement: true
    }
  }, function (error, result) {
    if (result) {
      res.status(200).send(result);
    } else {
      res.status(500).send(error);
    }
  });
}
// add new customers
const addCustomer = async (req, res) => {
  let nonceFromTheClient = req.body.paymentMethodNonce;
  let newTransaction = gateway.customer.create({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    payment_method_nonce: nonceFromTheClient
  }, function (error, result) {
    if (result) {
      console.log(firstName);
      res.status(200).send(result);
    } else {
      res.status(500).send(error);
    }
  });
}
// add card
const addcard = async (req, res) => {
  let nonceFromTheClient = req.body.paymentMethodNonce;
  let newTransaction = gateway.paymentMethod.create({
    customerId: "104963314",
    paymentMethodNonce: 'fake-valid-no-billing-address-nonce'
  }, function (error, result) {
    if (result) {
      res.status(200).send(result);
    } else {
      res.status(500).send(error);
    }
  });
}
// add card by manually with pci compliance
const addcardMan = async (req, res) => {
  let newTransaction = gateway.creditCard.create({
    customerId: "882369681",
    number: '4111111111111111',
    expirationDate: '06/2022',
    cvv: '100'
  }, function (error, result) {
    if (result) {
      res.status(200).send(result);
    } else {
      res.status(500).send(error);
    }
  });
}

module.exports = {
  getToken: getToken,
  checkout: checkout,
  addCustomer: addCustomer,
  customerFind: customerFind,
  addcard:addcard,
  addcardMan:addcardMan
};