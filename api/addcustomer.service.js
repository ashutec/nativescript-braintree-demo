const braintree = require("braintree");


var gateway = braintree.connect({
  environment: braintree.Environment.Sandbox,
  merchantId: "5w53fft33r2542f5",
  publicKey: "tpcgyh9m4s9wrgbb",
  privateKey: "1aa8105ecd7b446c47591b84c3555dd0"
});
//get token
const getToken = async (req, res) => {
  gateway.clientToken.generate({}, (err, response) => {
    res.status(200).send({ token: response.clientToken });
  });
}

// add new customers
const addCustomer = async (req, res) => {
  let newTransaction = gateway.customer.create({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
  }, function (error, result) {
    if (result) {
      res.status(200).send(result);
    } else {
      res.status(500).send(error);
    }
  });
}

module.exports = {
  getToken: getToken,
  addCustomer: addCustomer,
};