const braintree = require("braintree");


var gateway = braintree.connect({
  environment: braintree.Environment.Sandbox,
  merchantId: "5w53fft33r2542f5",
  publicKey: "tpcgyh9m4s9wrgbb",
  privateKey: "1aa8105ecd7b446c47591b84c3555dd0"
});

// add card
const addcard = async (req, res) => {
    let nonceFromTheClient = req.body.paymentMethodNonce;
    let newTransaction = gateway.paymentMethod.create({
      customerId: "104963314",
      paymentMethodNonce: 'fake-valid-no-billing-address-nonce'
    }, function (error, result) {
      if (result) {
        res.status(200).send(result);
      } else {
        res.status(500).send(error);
      }
    });
  }
  // add card by manually with pci compliance
  const addcardMan = async (req, res) => {
    let newTransaction = gateway.creditCard.create({
      customerId: req.body.customerId,  //"882367681"
      number: req.body.number, //'4111111111111111'
      expirationDate: req.body.EDate,  //'06/2022'
      cvv: req.body.cvv         //'100'
    }, function (error, result) {
      if (result) {
        res.status(200).send(result);
      } else {
        res.status(500).send(error);
      }
    });
  }


module.exports = {
  addcard:addcard,
  addcardMan:addcardMan,
};