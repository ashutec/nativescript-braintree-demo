const addcustomer = require('./addcustomer.service');
const addcarddetails = require('./addcarddetails.service');
const getcustomer = require('./getcustomer.service');
const paymentinit = require('./paymentinit.service');


module.exports = function(app) {
	app.get('/getToken', addcustomer.getToken);
	app.post('/addCustomer', addcustomer.addCustomer);
	app.post('/checkout', paymentinit.checkout);
	app.post('/customerFind', getcustomer.customerFind);
	app.post('/addcard', addcarddetails.addcard);
	app.post('/addcardman', addcarddetails.addcardMan);
};