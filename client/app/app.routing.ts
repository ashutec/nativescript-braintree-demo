import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { brainTreeComponent } from "./braintree/braintree.component"
import { paymentFormComponent } from "./paymentform/paymentfrom.component";
import { paymentCardComponent } from "./paymentcard/paymentcard.component";
import { paymentListComponent } from "./paymentlist/paymentlist.component";
import { enterCustomerComponent } from "./entercustomer/entercustomer.component";
import { customerDetailsComponent } from "./customerdetails/customerdetails.component";

const routes: Routes = [
    { path: "", redirectTo: "paymentcard", pathMatch: "full" },
    { path: "main", component: brainTreeComponent },
    { path: "paymentform", component: paymentFormComponent },
    { path: "paymentcard", component: paymentCardComponent },
    { path: "paymentlist", component: paymentListComponent },
    { path: "entercustomer", component: enterCustomerComponent },
    { path: "customerdetails", component: customerDetailsComponent },
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
