import { Component, OnInit } from '@angular/core';
import { Braintree, BrainTreeOptions, BrainTreeOutput } from 'nativescript-braintree';
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { url } from "../utils";
import * as localstorage from "nativescript-localstorage";
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
    selector: 'customerdetails',
    moduleId: module.id,
    templateUrl: 'customerdetails.component.html'
})

export class customerDetailsComponent implements OnInit {
    firstName;
    lastName;
    constructor(
        private routerExtension: RouterExtensions,
    ) { }

    ngOnInit(): void {
    }

    next() {
        localStorage.setItem('firstName', this.firstName);
        localStorage.setItem('lastName', this.lastName);
        this.routerExtension.navigate(["/paymentform"]);
    }

    clear() {
        localStorage.clear();
    }
}