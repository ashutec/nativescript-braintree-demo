import { Component, OnInit } from '@angular/core';
import { Braintree, BrainTreeOptions, BrainTreeOutput } from 'nativescript-braintree';
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { RouterExtensions } from "nativescript-angular/router";
var view = require("ui/core/view");

@Component({
  selector: 'paymentcard',
  moduleId: module.id,
  templateUrl: 'paymentcard.component.html'
})

export class paymentCardComponent implements OnInit {
  CustomerID
  braintree: Braintree;
  amount: string;
  token;
  firstName = "louis";
  lastName = "kal"
  url = 'http://72a21489.ngrok.io/'
  people;
  constructor(
    private http: HttpClient,
    private routerExtension: RouterExtensions,
  ) {
  }

  ngOnInit() {
    this.braintree = new Braintree();
  }

  pay() {
    this.routerExtension.navigate(["/entercustomer"]);
  }

  addpayment() {
    this.routerExtension.navigate(["/customerdetails"]);
  }

  callBrainTree() {
    let opts: BrainTreeOptions = {
      amount: '0',
      collectDeviceData: true,
      requestThreeDSecureVerification: false,
    }
    // get Token
    this.http.get(this.url + 'getToken').subscribe((resData: any) => {
      this.token = resData.token
      // brainTree Payment 
      this.braintree.startPayment(this.token, opts)
        .then((res: BrainTreeOutput) => {
          console.log(res);
          // Http call Nonce
          this.http.post(this.url + 'addCustomer', { firstName: this.firstName, lastName: this.lastName, paymentMethodNonce: res.nonce })
            .subscribe(res => {
              console.log(JSON.stringify(res));
            })
        }).catch((err) => {
          console.log('error');
          console.log(JSON.stringify(err));
        })
    })
  }
}