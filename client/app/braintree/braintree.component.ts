import { Component, OnInit } from '@angular/core';
import { Braintree, BrainTreeOptions, BrainTreeOutput } from 'nativescript-braintree';
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
// import { ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'brain-tree',
  moduleId: module.id,
  templateUrl: 'braintree.component.html'
})

export class brainTreeComponent implements OnInit {
  braintree: Braintree;
  constructor(private http: HttpClient) { }
  amount: string;
  token;
  url = 'http://72a21489.ngrok.io/'

  ngOnInit() {
    this.braintree = new Braintree();
  }

  callBrainTree() {
    let opts: BrainTreeOptions = {
      amount: this.amount,
      collectDeviceData: true,
      requestThreeDSecureVerification: false,
    }
    // get Token
    this.http.get(this.url + 'getToken').subscribe((resData:any)=>{
      this.token = resData.token
      // brainTree Payment 
        this.braintree.startPayment(this.token, opts)
          .then((res: BrainTreeOutput) => {
            // Http call Nonce
            this.http.post(this.url + 'checkout', { amount: this.amount, paymentMethodNonce: res.nonce, deviceInfo: res.deviceInfo })
              .subscribe(res => {
                console.log(JSON.stringify(res));
              })
          }).catch((err) => {
            console.log('error');
            console.log(JSON.stringify(err));
          })
    })
  }
}