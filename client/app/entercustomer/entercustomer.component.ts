import { Component, OnInit } from '@angular/core';
import { Braintree, BrainTreeOptions, BrainTreeOutput } from 'nativescript-braintree';
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { url } from "../utils";
import * as localstorage from "nativescript-localstorage";
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
    selector: 'entercustomer',
    moduleId: module.id,
    templateUrl: 'entercustomer.component.html'
})

export class enterCustomerComponent implements OnInit {
    customerID;
    constructor(
        private routerExtension: RouterExtensions,
    ) { }

    ngOnInit(): void {
    }

    enter() {
        localStorage.setItem('key', this.customerID);
        this.routerExtension.navigate(["/paymentlist"]);
    }

    clear() {
        localStorage.clear();
    }
}