import { Component, OnInit } from '@angular/core';
import { Braintree, BrainTreeOptions, BrainTreeOutput } from 'nativescript-braintree';
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
var view = require("ui/core/view");
import { url } from '../utils';
import * as localstorage from "nativescript-localstorage";
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
  selector: 'paymentform',
  moduleId: module.id,
  templateUrl: 'paymentform.component.html'
})

export class paymentFormComponent implements OnInit {
  firstName;
  lastName;
  cId;
  cardNo;
  date;
  cvv;
  constructor(
    private http: HttpClient,
    private routerExtension: RouterExtensions,
    ){}

  ngOnInit() {
    this.firstName = localstorage.getItem('firstName');
    this.lastName = localstorage.getItem('lastName');
  }
  
  addData() {
    this.http.post(url + 'addCustomer', { firstName: this.firstName, lastName: this.lastName })
      .subscribe(res => {
        const cId = res['customer'].id;
        localstorage.setItem('key',cId);
        this.http.post(url + 'addcardman', { customerId: cId, number: this.cardNo, EDate: this.date, cvv: this.cvv })
          .subscribe(res => {
            console.log(res);
            this.routerExtension.navigate(["/paymentcard"]);
          })
      })
  }
}
