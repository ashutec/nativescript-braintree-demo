import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AppComponent } from "./app.component";
import { brainTreeComponent } from "./braintree/braintree.component"
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptFormsModule } from "nativescript-angular/forms"
import { paymentFormComponent } from "./paymentform/paymentfrom.component";
import { Routes } from "@angular/router";
import { AppRoutingModule } from "./app.routing";
import { paymentCardComponent } from "./paymentcard/paymentcard.component";
import { paymentListComponent } from "./paymentlist/paymentlist.component";
import { TNSCheckBoxModule } from 'nativescript-checkbox/angular';
import { FormsModule,ReactiveFormsModule } from "@angular/forms";
import { enterCustomerComponent } from "./entercustomer/entercustomer.component";
import { customerDetailsComponent } from "./customerdetails/customerdetails.component";

@NgModule({
  declarations: [
    AppComponent,
    brainTreeComponent,
    paymentFormComponent,
    paymentCardComponent,
    paymentListComponent,
    enterCustomerComponent,
    customerDetailsComponent
  ],
  bootstrap: [AppComponent],
  imports: [
    NativeScriptModule,
    NativeScriptFormsModule,
    NativeScriptHttpClientModule,
    AppRoutingModule,
    TNSCheckBoxModule,
    ReactiveFormsModule,
    FormsModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule { }
