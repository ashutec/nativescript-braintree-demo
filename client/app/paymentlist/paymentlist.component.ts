import { Component, OnInit } from '@angular/core';
import { Braintree, BrainTreeOptions, BrainTreeOutput } from 'nativescript-braintree';
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { url } from "../utils";
import * as localstorage from "nativescript-localstorage";
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
  selector: 'paymentlist',
  moduleId: module.id,
  templateUrl: 'paymentlist.component.html'
})

export class paymentListComponent implements OnInit {
  amount;
  people = [];
  cardlist = [];
  args;
  me;
  checkoutCard;
  constructor(
    private http: HttpClient,
    private routerExtension: RouterExtensions,
  ) { }

  ngOnInit(): void {
    this.me = localstorage.getItem('key');
    this.http.post(url + 'customerFind', { customerId: this.me }).subscribe((res => {
      const data = res["creditCards"];
      data.map((res => {
        this.cardlist.push({ maskedNumber: res.maskedNumber, expirationDate: res.expirationDate, defaultMethod: false })
      }))
      this.people = this.cardlist;
    }));
  }


  onItemTap(event) {
    this.args = event
  }

  setDefaultPayment(item) {
    this.checkoutCard = item
    console.log(JSON.stringify(this.checkoutCard));
    this.people.map((res) => {
      if (res.maskedNumber === item.maskedNumber) {
        res.defaultMethod = true;
      } else {
        res.defaultMethod = false;
      }
    })
  }

  paymentCheckout() {
    this.http.post(url + 'checkout', { amount: this.amount, customerId: this.me, number: this.checkoutCard.maskedNumber, expirationDate: this.checkoutCard.maskedNumber })
      .subscribe((res => {
        console.log(JSON.stringify(res));
        this.routerExtension.navigate(["/paymentcard"]);
      }));
  }
}



    // this.people = [{ id: "0", maskedNumber: "12345678910", defaultMethod: true },
    // { id: "1", maskedNumber: "123456789", defaultMethod: false },
    // { id: "2", maskedNumber: "123456789", defaultMethod: false }];
