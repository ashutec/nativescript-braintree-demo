const http = require('http');
const express = require('express');
const app = express();
var bodyParser = require("body-parser");
const sms= require('./api/index');


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.set('port', '3003');
sms(app);
var server = http.createServer(app).listen(app.get('port'), function () {
    console.log(`api server on port:${app.get('port')}`);
    // logger.info(`Environment:${config.get('env')}`);    
});